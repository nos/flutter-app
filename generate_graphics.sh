#!/usr/bin/env bash

#appicon

inkscape assets/graphics/adaptive-icon.svg --export-png="assets/graphics/appicon.png"  --export-area 18:18:90:90   --export-height=1024 --export-width=1024 --without-gui

inkscape assets/graphics/adaptive-icon.svg --export-png="assets/graphics/android-adaptive-bg.png" --export-id=layer3 --export-id-only --export-area-page   --export-height=1024 --export-width=1024 --without-gui

inkscape assets/graphics/adaptive-icon.svg --export-png="assets/graphics/android-adaptive-fg.png" --export-id=layer1 --export-id-only --export-area-page   --export-height=1024 --export-width=1024 --without-gui

flutter pub run flutter_launcher_icons:main

import 'package:flutter/material.dart';

class FachschaftsItem {
  final IconData icon;
  final Color color;
  final List<String> names;

  FachschaftsItem(this.names, this.icon, this.color);
}

class Fachschaften {
  final Map<String, FachschaftsItem> _items;

  factory Fachschaften() => _instance;

  Fachschaften._mkInstance(List<FachschaftsItem> items)
      : _items = _mkItemList(items);

  static Map<String, FachschaftsItem> _mkItemList(List<FachschaftsItem> items) {
    Map<String, FachschaftsItem> itm = <String, FachschaftsItem>{};

    for (FachschaftsItem i in items) {
      for (String name in i.names) {
        itm[name] = i;
      }
    }

    return itm;
  }

  FachschaftsItem getItem(String fs) {
    fs = fs.toLowerCase();
    if (_items.containsKey(fs)) {
      return _items[fs];
    } else {
      return _items['default'];
    }
  }

  Color getColor(String fs) {
    return getItem(fs).color;
  }

  IconData getIcon(String fs) {
    return getItem(fs).icon;
  }

  static const int _intensity = 900;
  static final _instance = Fachschaften._mkInstance([
    FachschaftsItem(
        const ['default'], const _IconData(0xfffd), Colors.blue[_intensity]),
    FachschaftsItem(const ['inf', 'informatik'], const _IconData(0xe904),
        Colors.pink[_intensity]),
    FachschaftsItem(const ['biochemie'], const _IconData(0xe900),
        Colors.lightGreen[_intensity]),
    FachschaftsItem(
        const ['biologie', 'biowissenschaften', 'biophysik', 'biochemie'],
        const _IconData(0xe901),
        Colors.green[_intensity]),
    FachschaftsItem(
        const ['chemie'], const _IconData(0xe902), Colors.amber[_intensity]),
    FachschaftsItem(const ['geo', 'geowissenschaften'], const _IconData(0xe903),
        Colors.orange[_intensity]),
    FachschaftsItem(const ['mathematik'], const _IconData(0xe905),
        Colors.blueGrey[_intensity]),
    FachschaftsItem(const ['medizin', 'immunologie'], const _IconData(0xe906),
        Colors.cyan[_intensity]),
    FachschaftsItem(const ['pharmazie'], const _IconData(0xe907),
        Colors.greenAccent[_intensity]),
    FachschaftsItem(
        const ['physik'], const _IconData(0xe908), Colors.indigo[_intensity]),
    FachschaftsItem(
        const ['psychologie'], const _IconData(0xe909), Colors.red[_intensity]),
  ]);
}

class _IconData extends IconData {
  const _IconData(int codePoint)
      : super(codePoint, fontFamily: 'nightofscience');
}

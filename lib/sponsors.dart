import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class Sponsors extends StatelessWidget {
  static const List<String> sponsors = <String>[
    'aventis-foundation.jpg',
    "biotest.jpg",
    "celanese.png",
    "deutschebahn.png",
    "freunde-der-uni.jpg",
    "heraeus.png",
    "hertie-stiftung.jpg",
    "kuraray.png",
    "lyondellbasell.png",
    "newenglandbiolabs.png",
    "oswaltstiftung.png",
    "sanofi.png",
    "schott.png",
    "spardabank.png",
    "vetter.png",
  ];

  List<Widget> logos() {
    List<Widget> logos = [];
    for (String image in sponsors) {
      logos.add(Image.asset('assets/graphics/sponsors/$image'));
    }
    return logos;
  }

  @override
  Widget build(BuildContext context) {
    print(rootBundle);
    return Scaffold(
      appBar: AppBar(
        title: Text('Sponsoren'),
      ),
      body: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20.0),
          crossAxisSpacing: 10.0,
          crossAxisCount: 2,
          children: logos()),
    );
  }
}

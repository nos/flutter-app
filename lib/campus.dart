import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';


class CampusWidget extends StatelessWidget {
  static Color color = Colors.amber;
  static Icon icon = Icon(Icons.map);

  CampusWidget();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
        child: PhotoView(
          enableRotation: false,
          imageProvider: AssetImage("assets/graphics/lageplan.png"),
        )
    );
  }
}

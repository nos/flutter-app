import 'package:flutter/material.dart';
import 'data.dart';
import 'graphics.dart';

class ScheduleWidget extends StatefulWidget {
  static Color color = Colors.green;
  static Icon icon = Icon(Icons.timer);
  static const double px_per_minute = 4;
  static const double px_per_quarter = 15 * px_per_minute;

  @override
  _ScheduleWidgetState createState() => _ScheduleWidgetState();
}

class _ScheduleWidgetState extends State<ScheduleWidget>
    with SingleTickerProviderStateMixin {
  //TabController _tabController;
  ScrollController _scheduleScrollController;
  ScrollController _timeScaleScrollController = new ScrollController();
  final LectureData lectureData = LectureData();

  @override
  void initState() {
    super.initState();

    _scheduleScrollController = TrackingScrollController();
  }

  @override
  void dispose() {
    //_tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: lectureData.getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return snapshot.hasData
              ? Stack(
                  children: <Widget>[

                    Padding(
                        padding: const EdgeInsets.only(top: 100.0),
                        child: TimesSlider(_timeScaleScrollController,
                            lectureData.data.rooms[0])),
                    NotificationListener<ScrollNotification>(
                        child: PageView(
                          children: roomViews(),
                        ),
                        onNotification: (ScrollNotification scrollInfo) {
                          _timeScaleScrollController.jumpTo(
                              _scheduleScrollController.initialScrollOffset);
                        })
                  ],
                )
              : Center(child: CircularProgressIndicator());
        });
  }

  List<Widget> roomViews() {
    List<Widget> rv = [];
    for (Room room in lectureData.data.rooms) {
      rv.add(RoomSchedule.fromRoom(_scheduleScrollController, room));
    }
    return rv;
  }
}

class TimesSlider extends StatelessWidget {
  final ScrollController _scrollController;
  final Room room;

  TimesSlider(this._scrollController, this.room);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        controller: this._scrollController,
        itemCount: Lectures.lectureHours().length,
        itemBuilder: (context, index) => Column(children: <Widget>[
              Divider(height: 1),
              SizedBox(
                  height: ScheduleWidget.px_per_quarter - 1,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "${Lectures.lectureHours()[index].hour}:00",
                        style: Theme.of(context).textTheme.overline,
                        textAlign: TextAlign.left,
                      ))),
              SizedBox(
                  height: ScheduleWidget.px_per_quarter,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "${Lectures.lectureHours()[index].hour}:15",
                        style: Theme.of(context).textTheme.overline,
                        textAlign: TextAlign.left,
                      ))),
              SizedBox(
                  height: ScheduleWidget.px_per_quarter,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "${Lectures.lectureHours()[index].hour}:30",
                        style: Theme.of(context).textTheme.overline,
                        textAlign: TextAlign.left,
                      ))),
              SizedBox(
                  height: ScheduleWidget.px_per_quarter,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "${Lectures.lectureHours()[index].hour}:45",
                        style: Theme.of(context).textTheme.overline,
                        textAlign: TextAlign.left,
                      ))),
            ]));
  }
}

class RoomSchedule extends StatelessWidget {
  final ScrollController _scrollController;
  final Room room;

  final List<Lecture> roomSchedule;

  RoomSchedule(this._scrollController, this.room, this.roomSchedule);

  factory RoomSchedule.fromRoom(ScrollController scrollController, Room room) {
    List<Lecture> _roomSchedule = [];
    if (room.lectures.length > 0) {
      DateTime lastEnd = Lectures.startTime;
      DateTime nextStart;
      int i = 0;
      do {
        nextStart = room.lectures[i].start;
        Lecture sparse = Lecture.empty(start: lastEnd, end: nextStart);
        if (sparse != null) {
          _roomSchedule.add(sparse);
        }

        _roomSchedule.add(room.lectures[i]);
        lastEnd = room.lectures[i].end;
        i++;
      } while (i < room.lectures.length);
      Lecture sparse = Lecture.empty(start: lastEnd, end: Lectures.endTime);
      if (sparse != null) {
        _roomSchedule.add(sparse);
      }
    }

    return RoomSchedule(scrollController, room, _roomSchedule);
  }

  @override
  Widget build(BuildContext context) {
    print(Theme.of(context).brightness);
    return RefreshIndicator(
        onRefresh: LectureData().reload,
        child: CustomScrollView(
            controller: this._scrollController,
            slivers: <Widget>[
              SliverAppBar(
                  pinned: true,
                  floating: false,
                  primary: false,
                  automaticallyImplyLeading: false,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: Text(room.name),
                  )),
              SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (context, index) => Column(
                          children: <Widget>[LectureCard(roomSchedule[index])]),
                      childCount: roomSchedule.length))
            ]));
  }
}

class _LectureCardState extends State<LectureCard> {
  final Lecture l;

  _LectureCardState(this.l);

  double pxFromDureation(DateTime start, DateTime end) {
    int minutes = start.difference(end).inMinutes.abs();
    return ScheduleWidget.px_per_minute * minutes;
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextStyle titleStyle = theme.textTheme.subtitle;
    final TextStyle descriptionStyle = theme.textTheme.caption;
    final TextStyle nameStyle =
        theme.textTheme.subtitle.copyWith(color: descriptionStyle.color);

    if (!l.isEmpy()) {
      IconData ico = Fachschaften().getIcon(l.fs);
      return SizedBox(
          height: pxFromDureation(l.start, l.end),
          child: Container(
              padding: EdgeInsets.only(
                  left: 50.0, top: 0.0, right: 10.0, bottom: 0.0),
              child: Card(
                  color: Fachschaften().getColor(l.fs),
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                      child: DefaultTextStyle(
                        softWrap: true,
                        //overflow: TextOverflow.ellipsis,
                        style: descriptionStyle,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(bottom: 5.0),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    l.fs,
                                    style: descriptionStyle,
                                  ),
                                  Expanded(
                                      child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            "${l.room}: ${l.time}",
                                            style: descriptionStyle,
                                          )))
                                ],
                              ),
                            ),
                            Row(children: <Widget>[
                              Icon(
                                ico,
                                size: 50.0,
                                color: theme.hintColor,
                              ),
                              Expanded(
                                  child: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10.0),
                                      child: Column(children: <Widget>[
                                        Align(
                                            alignment: Alignment.centerLeft,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 4.0),
                                              child: Text(
                                                l.name,
                                                style: nameStyle,
                                              ),
                                            )),
                                        Align(
                                            alignment: Alignment.centerLeft,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 4.0),
                                              child: Text(
                                                l.teaser,
                                                style: titleStyle,
                                              ),
                                            ))
                                      ]))),
                            ]),
                            Spacer(),
                            ButtonTheme.bar(
                              //color:Colors.green,
                              // make buttons use the appropriate styles for cards
                              child: ButtonBar(
                                children: <Widget>[
                                  IconButton(
                                      icon: l.like
                                          ? Icon(Icons.favorite)
                                          : Icon(Icons.favorite_border),
                                      tooltip: 'blah',
                                      onPressed: () {
                                        setState(() {
                                          l.like = !l.like;
                                        });
                                      }),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )))));
    } else {
      return Container(height: pxFromDureation(l.start, l.end));
    }
  }
}

/*             Text(),
                    ListTile(
                        leading: Icon(
                          ico,
                          size: 50.0,
                          color: Colors.black,
                        ),

                        subtitle: Text(l.name)),
                    Padding(
                        padding: const EdgeInsets.only(left: 80.0),
                        child: Column(children: <Widget>[
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text()),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text))
                        ])),
                    Spacer(),*/

class LectureCard extends StatefulWidget {
  final Lecture lecture;

  LectureCard(this.lecture);

  @override
  _LectureCardState createState() => _LectureCardState(lecture);
}

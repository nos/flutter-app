import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Assets {
  Future<Map<String, dynamic>> loadJSON(file) async {
    String dataAsString = await rootBundle.loadString(file);
    return await jsonDecode(dataAsString);
  }

  Future<File> localFile(filename) async {
    final directory = await getApplicationDocumentsDirectory();

    return File('${directory.path}/$filename');
  }
}

class LectureData extends Assets {
  static final LectureData _instance = LectureData._internal();
  static const String lurl = 'https://nos.prause.de/lectures.json';

  factory LectureData() => _instance;

  Lectures _data;

  Lectures get data {
    return _data;
  }

  Future<Lectures> _dataFuture;

  Future<Lectures> get dataFuture {
    return _dataFuture;
  }

  LectureData._internal() {
    _dataFuture = getData();
    _dataFuture.then(_storeSchedule);
  }

  Future<Lectures> reload() async {
    print('reloading');
    DefaultCacheManager().emptyCache();
    DefaultCacheManager().downloadFile(lurl);
    return getData();
  }

  Future<Lectures> getData() async {
    //File file = await DefaultCacheManager().getSingleFile(lurl);
    var cacheFile = await DefaultCacheManager().getFileFromCache(lurl);

    String dataAsString;

    if (cacheFile == null) {
      print('got lectures from app');

      dataAsString = await rootBundle.loadString('assets/data/lectures.json');
    } else {
      print('got lectures from cache');
      dataAsString = await cacheFile.file.readAsString();
    }

    Map<String, dynamic> json = await jsonDecode(dataAsString);
    return Future(() {
      return Lectures.fromJSON(json);
    });
  }

  void _storeSchedule(scheduleData) {
    _data = scheduleData;
  }
}

class Lectures {
  String version;
  List<Room> rooms = [];

  static final DateTime startTime = DateTime(2019, 6, 14, 17, 0);
  static final DateTime endTime = startTime.add(Duration(hours: 12));

  static List<DateTime> lectureHours() {
    List<DateTime> times = [Lectures.startTime];
    int i = 1;
    DateTime nextTime;
    do {
      nextTime = times[i - 1].add(Duration(hours: 1));
      times.add(nextTime);
      i++;
    } while (nextTime.isBefore(Lectures.endTime));

    return times;
  }

  Lectures(final this.version, final this.rooms);

  Lectures.fromJSON(Map<String, dynamic> json) {
    version = json['version'];
    for (var ro in json['schedule']) {
      rooms.add(Room.fromJSON(ro));
    }
  }

  String toString() {
    return ('version: $version rooms: $rooms');
  }
}

class TimeSlot extends Comparable {
  String name;
  String begin;
  String end;
  Lecture lecture;

  TimeSlot(
      final this.name, final this.begin, final this.end, final this.lecture);

  TimeSlot.fromJSON(Map<String, dynamic> json) {
    name = json['time'];
    begin = json['begin'];
    end = json['end'];
    lecture = Lecture.fromJSON(json['lecture']);
  }

  @override
  int compareTo(other) {
    if (other is TimeSlot) {
      return name.compareTo(other.name);
    } else {
      return 1;
    }
  }

  String toString() {
    return ('$name lecture:$lecture');
  }
}

class Room {
  final String name;
  final List<TimeSlot> timeSlots;
  final List<Lecture> lectures;

  Room(final this.name, final this.timeSlots, this.lectures);

  factory Room.fromJSON(Map<String, dynamic> json) {
    String name = json['name'];
    List<TimeSlot> timeSlots = [];
    List<Lecture> lectures = [];
    for (var ti in json['times']) {
      TimeSlot ts = TimeSlot.fromJSON(ti);
      timeSlots.add(ts);
      if (ts.lecture != null) {
        lectures.add(ts.lecture);
      }
    }
    return Room(name, timeSlots, lectures);
  }

  String toString() {
    return ('room: $name times: $timeSlots');
  }
}

class Lecture {
  static const String _emptyID = 'placeholder';
  final String fs, name, teaser, time, room, id;
  final bool aufz;
  bool _like = false;
  SharedPreferences prefs;
  final DateTime start, end;

  bool get like => _like;

  set like(bool liked) {
    _like = liked;
    saveBoolPref(id, _like);
  }

  bool isEmpy() {
    return (id == Lecture._emptyID);
  }

  void readLike(String prefId, {bool valDefault = false}) {
    Future<SharedPreferences> prefs = SharedPreferences.getInstance();
    prefs.then((SharedPreferences p) {
      if (p.containsKey(prefId)) {
        _like = p.getBool(prefId);
      } else {
        _like = valDefault;
      }
    });
  }

  void saveBoolPref(String prefId, dynamic prefValue) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('saving $prefId $prefValue');
    prefs.setBool(prefId, prefValue);
  }

  set id(String lectureID) {}

  Lecture(
      final this.fs,
      final this.name,
      final this.teaser,
      final this.aufz,
      final this.time,
      final this.room,
      final this.start,
      final this.end,
      final this.id);

  factory Lecture.empty({DateTime start, DateTime end}) {
    if (!end.isAtSameMomentAs(start)) {
      return Lecture(
          null, null, null, null, null, null, start, end, Lecture._emptyID);
    } else {
      return null;
    }
  }

  @override
  String toString() {
    return "Lecture $id $start $end";
  }

  static DateTime fromDateandTime(DateTime date, String timeString) {
    timeString = timeString.trim();
    List<String> timeHM = timeString.split('.');
    int hours = int.parse(timeHM[0]);
    int minutes = int.parse(timeHM[1]);
    DateTime dt = DateTime(date.year, date.month, date.day, hours, minutes);
    if (hours >= date.hour) {
      return dt;
    } else {
      return dt.add(Duration(days: 1));
    }
  }

  factory Lecture.fromJSON(Map<String, dynamic> json) {
    if (json != null) {
      String time = json['time'];
      List<String> times = time.split('-');

      DateTime start = fromDateandTime(Lectures.startTime, times[0]);
      DateTime end = fromDateandTime(Lectures.startTime, times[1]);

      String lectureID = json['id'];
      if ((lectureID == null || lectureID == "") && json['teaser'] != null) {
        lectureID = json["teaser"]
            .toLowerCase()
            .replaceAll(new RegExp(r"\s+\b|\b\s|\s|\b"), "");
      }

      Lecture l = Lecture(
          json['fs'].trim(),
          json['name'].trim(),
          json['teaser'].trim(),
          json['aufz'],
          time,
          json['room'],
          start,
          end,
          json['id']);

      return l;
    } else {
      return null;
    }
  }
}

import 'package:flutter/material.dart';

//import 'news.dart';
//import 'tours.dart';
import 'campus.dart';
import 'schedule.dart';
import 'data.dart';
import 'sponsors.dart';
import 'graphics.dart';
import 'dart:ui' as ui;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Night of Science',
      theme: ThemeData.dark().copyWith(
          //primarySwatch: Colors.blue,
          //brightness: Brightness.dark,
          ),
      home: NoSHomePage(title: 'Night of Science'),
      initialRoute: '/',
      routes: {
        '/second': (context) => Sponsors(),
      },
    );
  }
}

class NoSHomePage extends StatefulWidget {
  NoSHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _NoSHomePageState createState() => _NoSHomePageState();
}

class _NoSHomePageState extends State<NoSHomePage> {
  int _selectedIndex = 0;
  LectureData lectureData = LectureData();

  final List<Widget> _children = [
    //NewsWidget(),
    //ToursWidget(),
    ScheduleWidget(),
    CampusWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    final double ratio = MediaQuery.of(context).devicePixelRatio;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    final double pheight = ui.window.physicalSize.height;
    final double pwidth = ui.window.physicalSize.width;

    print(
        "width: $width($pwidth) height: $height($pheight) ratio: $ratio");

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Icon(
                Fachschaften().getIcon('default'),
                size: 50.0,
              ),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/graphics/davinci_bg.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            ListTile(
              title: Text('Sponsoren'),
              onTap: () {
                Navigator.pushNamed(context, '/second');
                //Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: _children[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          //BottomNavigationBarItem( icon: NewsWidget.icon, title: Text('News'), backgroundColor: NewsWidget.color),
          //BottomNavigationBarItem(icon: ToursWidget.icon,title: Text('Tours'),backgroundColor: ToursWidget.color),
          BottomNavigationBarItem(
              icon: ScheduleWidget.icon,
              title: Text('Schedule'),
              backgroundColor: ScheduleWidget.color),
          BottomNavigationBarItem(
              icon: CampusWidget.icon,
              title: Text('Campus'),
              backgroundColor: CampusWidget.color),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}

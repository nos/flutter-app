#!/bin/sh +x

vcode=$(date  +%s)

flutter build apk --release --build-name=2019.1.5 --build-number=$vcode
flutter build appbundle --build-name=2019.1.5 --build-number=$vcode
cp build/app/outputs/apk/release/app-release.apk ~/git/plumbing/fdroid-repo/repo/nos.apk

git -C ~/git/plumbing/fdroid-repo/repo add nos.apk
git -C ~/git/plumbing/fdroid-repo/repo commit -m "nos $vcode"
git -C ~/git/plumbing/fdroid-repo/repo push
